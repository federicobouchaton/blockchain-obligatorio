const HDWalletProvider = require('truffle-hdwallet-provider');
const mnemonic = 'cry police entire priority initial transfer prosper artist obvious profit spoon bring';
const infuraUrl = 'https://rinkeby.infura.io/v3/a1f379c70dd54378b7e6755ddf7435ec';

module.exports = {
  networks: {
    development: {      
      host: 'localhost',
      port: 7545,
      network_id: '*',
      gas: 5000000
    },
    rinkeby: {
      provider: () => new HDWalletProvider(mnemonic, infuraUrl),
      network_id: 4
    }
  }
}