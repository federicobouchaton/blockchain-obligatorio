let Auction = artifacts.require("./Auction.sol");

module.exports = function(deployer) {
    let name = "Auction 1";
    let description = "Auction 1 descriptiton";
    let basePrice = 2000000000000000000;
    let minPrice = 3000000000000000000;
    let maxPrice = 10000000000000000000;
    let maxAcceptedOffers = 4;
    let isMinPricePublicParam = false;
    let isMaxPricePublicParam = true;
    let isMaxAcceptedOffersPublicParam = false;
    let materialHash = "wWCivujDXrUgzY93fKtXHUqGXjw=";
    let url = "http://larifamos.com";
    deployer.deploy(Auction,name,description,materialHash,url, 
        maxAcceptedOffers, basePrice, minPrice, maxPrice,
        isMinPricePublicParam,isMaxPricePublicParam,isMaxAcceptedOffersPublicParam);
};
