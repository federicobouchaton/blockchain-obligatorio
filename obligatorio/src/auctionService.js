

export class AuctionService {
    constructor(contract){
        this.contract = contract;
    }
    async getContractBalance(fromAddress) {
        return await this.contract.getContractBalance({from: fromAddress});
    }
    async getHigherBet() {
        return await this.contract.getHigherBet();
    }
    async getMinPrice(fromAddress) {
        return await this.contract.getMinPrice({from: fromAddress});
    }
    async getMaxPrice(fromAddress) {
        return await this.contract.getMaxPrice({from: fromAddress});
    }
    async getMaxAcceptedOffers(fromAddress) {
        return await this.contract.getMaxAcceptedOffers({from: fromAddress});
    }
    async makeBet(fromAddress, valueBet){
        return await this.contract.makeBet({from: fromAddress, value: valueBet });
    }
    async closeAuction(fromAddress){
        return await this.contract.closeAuctionOwner({from: fromAddress});
    }
    async getAllBets(fromAddress) {
        var bets = await this.contract.getAllBets({from: fromAddress});
        let address = bets[0];
        let amount =bets[1];
        var returnBets = [];
        for (var i = 0; i < address.length; i++){
            var bet = {address: address[i], amount: amount[i].toNumber() };
            returnBets.push(bet);
        }
        return returnBets;
    }
    async isAuctionClosed() {
        return await this.contract.getAuctionIsCLosed();
    }
    isAuctionClosedPromise(){
        return new Promise( (resolve,reject) => {
            resolve(this.contract.getAuctionIsCLosed());
        }) 
    }
}