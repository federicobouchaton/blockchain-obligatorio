import AuctionContract from "../build/contracts/Auction.json";
import contract from "truffle-contract";

export default async(provider) => {
    const auction= contract(AuctionContract);
    auction.setProvider(provider);

    let instance = await auction.deployed();
    return instance;
};