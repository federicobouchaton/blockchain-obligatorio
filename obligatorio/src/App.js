import React,  { Component }  from "react";
import ReactDOM from 'react-dom';
import Panel from "./Panel";
import getWeb3 from "./getWeb3";
import AuctionContract from "./auction";
import {AuctionService} from "./auctionService";
import { ToastContainer}  from "react-toastr";

const converter= (web3) => {
    return (value) => {
        return web3.utils.fromWei(value.toString(),'ether');
    }
}


export class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valueBet: 0,
            bets: [],
            balance: 0,
            account: undefined, 
            higherBet: 0,
            minPrice: '',
            maxPrice: '',
            maxAcceptedOffers: '',
            contractBalance: '',
            auctionStatus: 'Open',
        };
        this.handleChange = this.handleChange.bind(this);
        
    }
   //funciones de front
    handleChange(event) {
        this.setState({valueBet: event.target.value});
    }
    wrapperError (msgError) {
        return msgError.split('revert')[1]
    }
    
    async makeBet(){
        try{
            await this.auctionService.makeBet(this.state.account, this.web3.utils.toWei(this.state.valueBet.toString(),'ether'))
            .then( response => {
                this.setState({ valueBet: 0 });
                this.auctionService.isAuctionClosedPromise()
                .then( response => {
                    if(response){
                        this.setState({ auctionStatus: 'Closed' });
                    }
                })
            });
        }catch(e){
            console.log(e.message);
                this.container.error(this.wrapperError(e.message));   
        }
    }
    async closeAuction(){
        try{
            await this.auctionService.closeAuction(this.state.account)
            .then( response => {
                this.setState({ valueBet: 0 });
                this.auctionService.isAuctionClosedPromise()
                .then( response => {
                    if(response){
                        this.setState({ auctionStatus: 'Closed' });
                    }
                })
            });
        }catch(e){
                this.container.error(this.wrapperError(e.message));
        }
    }
    async componentDidMount(){
        this.web3 = await getWeb3();
        this.toEher = converter(this.web3);
        this.auction = await AuctionContract(this.web3.currentProvider);
        this.auctionService = new AuctionService(this.auction);

        var account = (await this.web3.eth.getAccounts())[0];
        this.setState({
            account: account.toLowerCase()
        }, () => {
            this.load();
        }); 

        var betMade = this.auction.BetMade();
        betMade.watch(function(err, result){
            const  {client, amount} = result.args;
            this.container.success('New bet from '+ client + ' with amount of ' + this.toEher(amount) + ' ETH', 'Event information');
        }.bind(this));

        this.web3.currentProvider.publicConfigStore.on('update', async function (event){
            this.setState({
                account: event.selectedAddress.toLowerCase()
            }, () => {
                this.load();
            });
        }.bind(this));
        
    }
    async getBalance(){
        let weiBalance = await this.web3.eth.getBalance(this.state.account);
        this.setState({
            balance: this.toEher(weiBalance)
        });
    }

    async getHigherBet(){
        let weiBalance = await this.auctionService.getHigherBet();
        this.setState({higherBet: this.toEher(weiBalance)});
    }
    async getMinPrice(){
        try {
            let weiBalance = await this.auctionService.getMinPrice(this.state.account);
            this.setState({minPrice: this.toEher(weiBalance) + ' ETH' });
            ReactDOM.render(this.toEher(weiBalance) + ' ETH' , document.getElementById('divMinPrice'));
        }catch(e){
            ReactDOM.render(<span className="badge badge-danger">It is not public</span>, document.getElementById('divMinPrice'));
        }
       
    }
    async getMaxPrice(){
        try {
            let weiBalance = await this.auctionService.getMaxPrice(this.state.account);
            this.setState({maxPrice: this.toEher(weiBalance) + ' ETH' });
            ReactDOM.render(this.toEher(weiBalance) + ' ETH' , document.getElementById('divMaxPrice'));
        }catch(e){
            ReactDOM.render(<span className="badge badge-danger">It is not public</span>, document.getElementById('divMaxPrice'));
        }
       
    }
    async getMaxAcceptedOffers(){
        try {
            let weiBalance = await this.auctionService.getMaxAcceptedOffers(this.state.account);
            this.setState({maxAcceptedOffers: this.toEher(weiBalance) + ' ETH' });
            ReactDOM.render(this.toEher(weiBalance) + ' ETH' , document.getElementById('divMaxAcceptedOffers'));
        }catch(e){
            ReactDOM.render(<span className="badge badge-danger">It is not public</span>, document.getElementById('divMaxAcceptedOffers'));
        }
       
    }
    async getContractBalance(){
        try {
            let weiBalance = await this.auctionService.getContractBalance(this.state.account);
            this.setState({contractBalance: this.toEher(weiBalance) + ' ETH' });
            ReactDOM.render(this.toEher(weiBalance) + ' ETH' , document.getElementById('divContractBalance'));
        }catch(e){
            console.log(e.message);
            ReactDOM.render(<span className="badge badge-info">It is not owner</span>, document.getElementById('divContractBalance'));
        }
       
    }
    async getAuctionStatus(){
        this.auctionService.isAuctionClosedPromise()
        .then( response => {
            if(response){
                this.setState({ auctionStatus: 'Closed' });
                ReactDOM.render(<span className="badge badge-danger">{this.state.auctionStatus}</span>, document.getElementById('divAuctionStatus'));
            }else{
                ReactDOM.render(<span className="badge badge-success">{this.state.auctionStatus}</span>, document.getElementById('divAuctionStatus'));
            }
        })
       
    }
    async getAllBets(){
        try {
            let allBets = await this.auctionService.getAllBets(this.state.account);
            this.setState({ bets: allBets });
        } catch(e){
            this.setState({ bets: []});
        }
    }

    async load(){
        this.getBalance();
        this.getHigherBet();
        this.getMinPrice();
        this.getMaxPrice();
        this.getMaxAcceptedOffers();
        this.getAllBets();
        this.getContractBalance();
        this.getAuctionStatus();
    }

    render() {
        return <React.Fragment>
            <div className="jumbotron">
                <h4 className="display-4">Welcome to the Auction!</h4>
            </div>

            <div className="row">
                <div className="col-sm">
                    <Panel title="Contract information ">
                        <table className="table">
                            <tbody>
                            <tr>
                                <td><b>Auction Status: </b></td>
                                <td><div id="divAuctionStatus"></div></td>
                            </tr>
                            <tr>
                                <td><b>Contract balance: </b></td>
                                <td><div id="divContractBalance"></div></td>
                            </tr>
                            <tr>
                                <td><b>Higher bet: </b></td>
                                <td>{this.state.higherBet} ETH</td>
                            </tr>
                            <tr>
                                <td><b>Min price: </b></td>
                                <td><div id="divMinPrice"></div></td>
                            </tr>
                            <tr>
                                <td><b>Max price: </b></td>
                                <td><div id="divMaxPrice"></div></td>
                            </tr>
                            <tr>
                                <td><b>Max accepted offers: </b></td>
                                <td><div id="divMaxAcceptedOffers"></div></td>
                            </tr>
                            
                            
                            </tbody>
                        </table> 
                    </Panel>
                </div>
                 <div className="col-sm">
                
                    <Panel title="Make bet with account Metamask">
                        <p><b>{this.state.account}</b></p>
                        <span><b>Balance: </b>{this.state.balance} ETH</span>
                        <hr></hr>
                            <label> Bet: 
                            <input type="text" value={this.state.valueBet} onChange={this.handleChange}/>
                            </label>
                            <button className="btn btn-sm btn-success text-white " onClick={() => this.makeBet()}>Make bet </button>
                    </Panel>
                    <Panel title="Only actions of the owner and all bets">
                        <button className="btn btn-sm btn-danger text-white " onClick={() => this.closeAuction()}>Close auction </button>
                        <hr></hr>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>Address</th>
                                    <th>Amount</th> 
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.bets.map((bet,i) => {
                                    return <tr key={i}>
                                            <td>{bet.address}</td>
                                            <td>{this.toEher(bet.amount)} ETH </td>
                                        </tr>         
                            })}
                            </tbody>
                        </table>
                    </Panel>
                </div>
            </div>
           
            <div className="row">
                <div className="col-sm">
                   
                </div>
            </div>
            <ToastContainer ref={(input) =>this.container =input}
                className="toast-top-center"/>
        </React.Fragment>
    }
}