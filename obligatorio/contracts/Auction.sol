pragma solidity ^0.4.24;

contract Auction {
    address private owner;
    string materialHash;
    string digitalMaterialUrl;
    string name;
    string description;
    uint basePrice;
    uint minPrice;
    uint maxPrice;
    uint maxAcceptedOffers;
    bool isMinPricePublic;
    bool isMaxPricePublic;
    bool isMaxAcceptedOffersPublic;
    bool isOpen;
    bool isClosed;

    Bet[] public betsList;

    event BetMade(address client, uint amount);

    struct Bet{
        address gambler;
        uint amount;
    }

    constructor(string nameParam,
                    string descriptionParam,
                    string materialHashParam,
                    string digitalMaterialUrlParam,
                    uint maxAcceptedOffersParam,
                    uint basePriceParam,
                    uint minPriceParam,
                    uint maxPriceParam,
                    bool isMinPricePublicParam,
                    bool isMaxPricePublicParam,
                    bool isMaxAcceptedOffersPublicParam) public payable{
        owner = msg.sender;
        name = nameParam;
        description = descriptionParam;
        materialHash = materialHashParam;
        digitalMaterialUrl = digitalMaterialUrlParam;
        isOpen = true;
        isClosed = false;
        basePrice = basePriceParam;
        minPrice = minPriceParam;
        maxPrice = maxPriceParam;
        maxAcceptedOffers = (maxAcceptedOffersParam == 0) ? 1 : maxAcceptedOffersParam;
        isMinPricePublic = isMinPricePublicParam;
        isMaxPricePublic = isMaxPricePublicParam;
        isMaxAcceptedOffersPublic = isMaxAcceptedOffersPublicParam;
    }

    function makeBet() public payable isAuctionOpen isAuctionNotClosed {
        require(msg.value > basePrice,"Your bet should be more than the base price");
        require(msg.sender != getHigherBetOwner(),"Your bet is the highest");
        require(msg.value > getHigherBet(),"Your bet is not sufficient to pass the current one");

        //Send event
        emit BetMade(msg.sender, msg.value);

        if(betsList.length > 0){
            getHigherBetOwner().transfer(getHigherBet());
        }
        betsList.push(Bet({gambler:msg.sender,amount:msg.value}));
        maxAcceptedOffers--;
        if(maxAcceptedOffers == 0 || getHigherBet() > maxPrice){
            closeAuction();
        }
    }

    function getHigherBetOwner() private view returns (address) {
        if(betsList.length > 0){
            return betsList[betsList.length - 1].gambler;
        }
        return address(0);
    }

    function getHigherBet() public view returns (uint) {
        if(betsList.length > 0){
            return betsList[betsList.length - 1].amount;
        }
        return 0;
    }

    function closeAuctionOwner() public isOwner returns (bool) {
        require(getHigherBet() > minPrice,"No bet is higher than the minimum price");
        return closeAuction();
    }

    function closeAuction() private isAuctionOpen isAuctionNotClosed returns (bool){
        require(betsList.length > 0,"No bets done yet");
        isClosed = true;
        owner.transfer(address(this).balance);
    }

    function getAllBets() public isOwner view returns (address[], uint[]) {
        address[] memory gamblers = new address[](betsList.length);
        uint[] memory amounts = new uint[](betsList.length);
        for(uint i = 0; i < betsList.length; i++){
            gamblers[i] = betsList[i].gambler;
            amounts[i] = betsList[i].amount;
        }
        return (gamblers,amounts);
    }

    function getBet(uint position) public isOwner view returns (address, uint) {
        return (betsList[position].gambler, betsList[position].amount);
    }

    function getContractBalance() public isOwner view returns (uint) {
        return address(this).balance;
    }

    function getMinPrice() public view returns (uint) {
        require(isMinPricePublic,"Minimum price is not public in this Auction");
        return minPrice;
    }

    function getMaxPrice() public view returns (uint) {
        require(isMaxPricePublic,"Maximum price is not public in this Auction");
        return maxPrice;
    }

    function getMaxAcceptedOffers() public view returns (uint) {
        require(isMaxAcceptedOffersPublic,"Maximum offers count is not public in this Auction");
        return maxAcceptedOffers;
    }

    function getAuctionIsCLosed() public view returns (bool) {
        return isClosed;
    }

    modifier isOwner() {
        require(msg.sender == owner, "You should be the contract owner");
        _;
    }

    modifier isAuctionOpen() {
        require(isOpen, "The Auction should be open");
        _;
    }

    modifier isAuctionNotClosed() {
        require(!isClosed,"Auction is closed");
        _;
    }
}